# qwixx-typst

typst template for the german version of the dice game qwixx.
Meant as a starting point to design your own blocks (like a truly random one).

I am not affiliated with the game in any way. This is a project I made for fun to create a truly random sheet for myself.
