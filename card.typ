#let light-gray  = gray.lighten(20%)
#let dark-gray   = gray.darken(15%)
#let stroke-gray = gray.darken(40%)

#let red    = rgb("#932423")
#let yellow = rgb("#cea729")
#let green  = rgb("#287a3f")
#let blue   = rgb("#234576")

#let qwixx-colors = (
  red: red,
  yellow: yellow,
  green: green,
  blue: blue,
)

#set page(
  width: 12cm,
  height: 7.5cm,
  margin: 3mm,
  background: {
    set rect(width: 100%, outset: 0cm)
    place(top, rect(height: 100%-1.2cm, fill: gray))
    place(bottom, rect(height: 1.2cm, fill: dark-gray))
  }
)

#let block-spacing = 1mm
#let qwixx-box-stroke = 0.3mm + black
//#let qwixx-line = block.with(height: 100%/6 - block-spacing - qwixx-box-stroke.thickness*2)
#let qwixx-line = block.with(height: 1.1cm)
#let qwixx-box = box.with(width: 100%, height: 100%, stroke: qwixx-box-stroke, radius: 20%)
#let score-box = box.with(stroke: stroke-gray, fill: light-gray, radius: 20%, inset: (y: 0.5em, x: 0.3em))

#set block(spacing: block-spacing)
#set text(font: "Open Sans", size: 8pt)
#set stack(dir: ttb, spacing: block-spacing)

// field is a row, column indexed matrix containing dicts with at least:
// - number
// - color
#let card(field) = page({
  for line in field {
    qwixx-line({
      set align(center+horizon)
      set text(weight: "bold", size: 12pt)

      for (i, x) in line.enumerate() {
        let c = qwixx-colors.at(x.color)
        let c-light = c.mix((white, 95%))
        let c-dark = c.darken(50%)

        if i == 0 {
          box(height: 100%, width: 0.2fr, fill: c)
        }

        box(
          width: 1fr,
          height: 100%,
          fill: c,
          qwixx-box(
            height: 0.9cm,
            width: 100%-qwixx-box-stroke.thickness,
            stroke: c-dark,
            fill: c-light,
            text(fill: c, [#x.number])
          )
        )

        if i == line.len()-1 {
          box(height: 100%, width: 1fr, fill: c,
            circle(
              fill: c-light,
              stroke: c-dark,
              radius: 0.3cm,
              emoji.lock.open,
            )
          )
        }
      }
    })
  }

  qwixx-line(height: 1cm, {
    set align(center+horizon)
    set line(length: 100%, stroke: 0.1mm+stroke-gray)

    score-box(
      width: 1cm,
      stroke: none,
      fill: none,
      stack(dir: ttb,
        [ Kreuze ],
        line(),
        [Punkte ],
      )
    )

    let points = 0
    for x in range(1, 13) {
      points += x
      h(block-spacing, weak: true)
      score-box(
        width: 1fr,
        stack(
          dir: ttb,
          [ #x;x ],
          line(),
          [ #points ],
        )
      )
      h(block-spacing, weak: true)
    }

    score-box(
      width: 2.2cm,
      stroke: none,
      fill: none,
      stack(dir: ttb,
        [ Fehlwürfe je \-5 ],
        h(1fr),
        stack(dir: ltr, spacing: block-spacing,
          ..for x in range(4) {( score-box(width: 0.8em, height: 40%) ,)}
        ),
      ),
    )
  })

  qwixx-line(height: auto, {
    set align(horizon)
    let score-box = score-box.with(height: 2em, width: 3em)
    v(1fr)
    stack(dir: ltr, spacing: 1fr,
      [Ergebnis],
      score-box(stroke: red),
      text(weight: "bold", fill: stroke-gray)[+],
      score-box(stroke: yellow),
      text(weight: "bold", fill: stroke-gray)[+],
      score-box(stroke: green),
      text(weight: "bold", fill: stroke-gray)[+],
      score-box(stroke: blue),
      text(weight: "bold", fill: stroke-gray)[-],
      score-box(stroke: stroke-gray),
      text(weight: "bold", fill: stroke-gray)[=],
      score-box(width: 9em, stroke: black),
    )
    v(3fr)
  })
})

#let default-dimensions = (x: 11, y: 4)
#let default-field = qwixx-colors.keys().zip((
  range(2, default-dimensions.x+2),
  range(2, default-dimensions.x+2),
  range(12, 12-default-dimensions.x, step: -1),
  range(12, 12-default-dimensions.x, step: -1),
)).map(
  ((c, numbers)) => {
    numbers.map(n => (number: n, color: c))
  }
)

#card(yaml("default-card.yaml"))
#card(yaml("reversed-card.yaml"))
#card(yaml("shuffled-card.yaml"))
